const doubleNumber = num => num * 2;

module.exports = doubleNumber;